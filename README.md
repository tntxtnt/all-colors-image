# All Colors Image

[![build status](https://gitlab.com/tntxtnt/all-colors-image/badges/master/build.svg)](https://gitlab.com/tntxtnt/all-colors-image/commits/master)

- Inspiration: [I made a site that generates images where every pixel is a different color](https://theomg.github.io/omnichroma/). [(reddit thread)](https://redd.it/6rc2xv)
- C++ adaptation from source on codegolf: [Images with all colors](https://codegolf.stackexchange.com/a/22326)

## Samples
![Min](image/1500000000-min.png)  
min

![Average](image/1500000000-avg.png)  
average

## Build
Dependency: libpng

Run `make` to build `bin/acimg`

Alternatively, run `g++ -std=c++1z -O3 -Wall src/main.cpp src/lodepng.cpp -s  -o acimg` 

## Usage
```
Usage: acimg [-n 32] [-p 5] [-d m]

   -n [16|32|64|128|256] (default: 32)
        16 -->   64 x   64 image (~instant)
        32 -->  256 x  128 image (~seconds)
        64 -->  512 x  512 image (~minutes)
       128 --> 2048 x 1024 image (~hours)
       256 --> 4096 x 4096 image (~days)

   -p [1-9] (default: 5)
       Starting point
         1   2   3
         4   5   6
         7   8   9

   -d [m|a] (default: m)
       Strategy to calculate point-color difference
         m = min (fast but grainy)
         a = avg (slow)

   -s [0-4294967295] (default: time(0))
       Seed
```

## License
MIT

## Credits

- fejesjoco's implementation on codegolf: [Images with all colors](https://codegolf.stackexchange.com/a/22326)